package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class CartController {

    @Autowired
    private Map<Integer,Item> catalog;

    @Autowired
    private CartService cartService;

    @RequestMapping("/")
    public String showCatalog(){ return "catalog";}

}
