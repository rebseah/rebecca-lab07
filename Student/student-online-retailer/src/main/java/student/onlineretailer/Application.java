package student.onlineretailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
//@Profile("development")
public class Application {

	public static void main(String[] args) {

		ApplicationContext ctx = SpringApplication.run(Application.class, args);
		CartService service = ctx.getBean(CartServiceImpl.class);

		System.out.printf("Retailer email: %s \n", ((CartServiceImpl) service).getContactEmail());
		System.out.printf("This is the sales tax rate %f\n", ((CartServiceImpl) service).getSalesTaxRate());
		System.out.printf("This is the delivery charge on normal days %f\n", ((CartServiceImpl) service).getNormalDelivery());
		System.out.printf("This is the delivery charge threshold %f\n", ((CartServiceImpl) service).getThresholdDelivery());

		service.addItemToCart(0, 3);
		service.addItemToCart(1,1);
		service.addItemToCart(2,2);
		service.addItemToCart(3,1);
		service.getAllItemsInCart();
		double totalCost = service.calculateCartCost();
		System.out.println("Everything cost " + totalCost);

		// put resources bean
		ResourcesBean resources = ctx.getBean(ResourcesBean.class);
		System.out.println("Resources info in yaml file"+ resources);


	}


	@Bean
	public Map<Integer, Item> catalog(){
		Map <Integer, Item> items = new HashMap<>();
		Item redPen = new Item (0, "Red Pen", 2.20);
		Item greenPen = new Item (1, "Green Pen", 3.40);
		Item bluePen = new Item (2, "Blue Pen", 2.80);
		Item pencilCase = new Item (3, "Pencil Case", 4.70);
		items.put(redPen.getId(), redPen);
		items.put(greenPen.getId(), greenPen);
		items.put(bluePen.getId(), bluePen);
		items.put(pencilCase.getId(), pencilCase);
		return items;
	}
}
